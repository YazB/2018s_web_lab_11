-- Answers to Exercise 2 here

DROP TABLE IF EXISTS exercise_two;

CREATE TABLE exercise_two (
  username VARCHAR(20),
  fname VARCHAR(50),
  lname VARCHAR(100),
  email VARCHAR(50)
);

INSERT INTO exercise_two VALUES ('sunflower', 'Peter','Frankerson','peterfrankerson@gmail.com');
INSERT INTO exercise_two VALUES ('hiccup', 'Sam','Fowler','sammyf123@gmail.com');
INSERT INTO exercise_two VALUES ('rainbows', 'Bob','Dylan','bbdylan@slingshot.co.nz');
INSERT INTO exercise_two VALUES ('snowflake', 'Pete','Phoney','prettypete@waikato.ac.nz');
INSERT INTO exercise_two VALUES ('trace', 'Tracey','Turnip','triplethreatTracey@outlook.com');
INSERT INTO exercise_two VALUES ('lego', 'Pumpkin','Smith','thepumpkinsmiths@gmail.com');
INSERT INTO exercise_two VALUES ('balloon', 'Beatrice','Balloon','btotheskies@hcc.govt.nz');
INSERT INTO exercise_two VALUES ('snake', 'Alice','Peterson','alwaysalice@yahoo.com');
INSERT INTO exercise_two VALUES ('cracker', 'David','Bridge','davidbridge@gmail.com');
INSERT INTO exercise_two VALUES ('sunshine', 'Violet','Victory','violetvictory@slingshot.co.nz');
INSERT INTO exercise_two VALUES ('sunshine', 'Yasmin','Brinkworth','yab2@students.waikato.ac.nz');

