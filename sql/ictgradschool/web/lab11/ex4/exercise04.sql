-- Answers to Exercise 4 here
DROP TABLE IF EXISTS exercise_four;

CREATE TABLE  exercise_four(
  id integer(4),
  title varchar(30),
  article text,
  PRIMARY KEY (id)
);

INSERT INTO exercise_four VALUES ('1111', 'Lorem ipsum dolor sit amet', 'consectetur adipiscing elit. Sed a justo eget ligula tempor gravida. Suspendisse potenti. Mauris ut porttitor risus. Aliquam a venenatis massa. In dictum interdum augue porttitor dignissim. Nam faucibus erat ut enim varius dignissim. Donec lacinia felis urna. Morbi euismod eleifend leo, et molestie enim luctus in. Donec ut consequat enim, hendrerit commodo metus. Phasellus vitae cursus lacus. Ut in magna ultricies, sollicitudin nulla ut, tempor turpis. Donec elementum bibendum neque in tempor. Phasellus mattis euismod ipsum, id viverra nunc venenatis id');

INSERT INTO exercise_four VALUES  ('1234', 'Praesent euismod cursus diam', 'eget pulvinar nisi condimentum ac. Nam vehicula tristique purus non pulvinar. Curabitur fermentum scelerisque quam, a consectetur libero blandit vitae. Donec dui orci, aliquam non urna nec, ultricies gravida tellus. Donec sed augue vitae turpis pellentesque semper. Aliquam erat volutpat. Cras dignissim urna et egestas hendrerit. Mauris sit amet nisi augue. Donec laoreet congue sapien at ornare. Aenean quis nunc sit amet sapien tincidunt ullamcorper eu sit amet est. Vivamus bibendum vitae lacus quis pellentesque. Praesent pharetra mi vitae justo elementum, sit amet viverra metus ullamcorper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac semper elit. Pellentesque a libero et neque aliquam iaculis');