-- Answers to Exercise 9 here
SELECT * from exercise_six;
SELECT barcode, title, director, hire_cost, member from exercise_six;
SELECT title from exercise_six;
SELECT DISTINCT director from exercise_six;
SELECT title from exercise_six WHERE hire_cost<2;
SELECT username from exercise_five;
SELECT fname from exercise_five WHERE fname LIKE 'Pete%';
SELECT fname OR lname from exercise_five WHERE fname OR lname like 'Pete%';