-- Answers to Exercise 6 here
DROP TABLE IF EXISTS exercise_six;

CREATE TABLE exercise_six (
  barcode   integer(9),
  title     varchar(50),
  director  varchar(30),
  hire_cost integer(1),
  member integer not null ,
  primary key (barcode),
  FOREIGN KEY (member) REFERENCES exercise_three (id)
);

INSERT INTO exercise_six (barcode, title, director, hire_cost, member)
VALUES ('239485023', 'Zoolander', 'Ben Stiller', '2', (SELECT id FROM exercise_three where exercise_three.name='Peter Jackson') ),
       ('348598815', 'Grease', 'Randal Kleiser', '4', 2),
       ('554702190', 'Death on the Nile', 'John Guillermin', '2', 3),
       ('437553023', 'Raiders of the Lost Ark', 'Steven Spielberg', '6', 4),
       ('347890258', 'Howls Moving Castle', 'Hayao Miyazaki', '4', 5),
       ('459982349', 'The Golden Compass', 'Chris Weitz', '2', 5),
       ('132803485', 'A New Hope', 'George Lucas', '6', 5),
       ('340956842', 'Ready Player One', 'Steven Spielberg', '2', 8),
       ('548390028', 'Keanu', 'Peter Atencio', '4', 4),
       ('459038829', 'Eragon', 'Stefan Fangmeier', '4',10);

# DROP TABLE IF EXISTS  rental;
#
# CREATE table rental (
#   id integer NOT NULL auto_increment,
#   name varchar(50),
#   title varchar(50),
#   -- exercise_six_director varchar(30),
#   PRIMARY KEY (id),
#   FOREIGN KEY (title) REFERENCES exercise_six (title),
#   FOREIGN KEY (name) REFERENCES exercise_three (name)
#
# );