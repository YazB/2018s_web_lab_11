-- Answers to Exercise 7 here
DROP TABLE IF EXISTS exercise_seven;

CREATE TABLE exercise_seven(
  comments_id integer NOT NULL AUTO_INCREMENT,
  comments text,
  article_id integer NOT NULL,
  PRIMARY KEY (comments_id),
  FOREIGN KEY (article_id) REFERENCES exercise_four (id)
);

INSERT INTO exercise_seven (comments, article_id)
VALUES ('Super awesome!', '1111'),
       ('Terrible writing. Get a new editor', '1234');