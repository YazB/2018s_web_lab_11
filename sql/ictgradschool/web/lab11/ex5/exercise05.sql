-- Answers to Exercise 5 here
DROP TABLE IF EXISTS exercise_five;

CREATE TABLE exercise_five (
  number INTEGER (2),
  username VARCHAR(20),
  fname VARCHAR(50),
  lname VARCHAR(100),
  email VARCHAR(50),
  PRIMARY KEY (username)
);

INSERT INTO exercise_five VALUES (1, 'sunflower', 'Peter','Frankerson','peterfrankerson@gmail.com');
INSERT INTO exercise_five VALUES (2, 'hiccup', 'Sam','Fowler','sammyf123@gmail.com');
INSERT INTO exercise_five VALUES (3, 'rainbows', 'Bob','Dylan','bbdylan@slingshot.co.nz');
INSERT INTO exercise_five VALUES (4, 'snowflake', 'Pete','Phoney','prettypete@waikato.ac.nz');
INSERT INTO exercise_five VALUES (5, 'trace', 'Tracey','Turnip','triplethreatTracey@outlook.com');
INSERT INTO exercise_five VALUES (6, 'lego', 'Pumpkin','Smith','thepumpkinsmiths@gmail.com');
INSERT INTO exercise_five VALUES (7, 'balloon', 'Beatrice','Balloon','btotheskies@hcc.govt.nz');
INSERT INTO exercise_five VALUES (8, 'snake', 'Alice','Peterson','alwaysalice@yahoo.com');
INSERT INTO exercise_five VALUES (9, 'cracker', 'David','Bridge','davidbridge@gmail.com');
INSERT INTO exercise_five VALUES (10, 'sunshine', 'Violet','Victory','violetvictory@slingshot.co.nz');
INSERT INTO exercise_five VALUES (11, 'balloon', 'Bobby','Bunny','bobbythebunny246@gmail.com');