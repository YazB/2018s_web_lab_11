-- Answers to Exercise 3 here

DROP TABLE IF EXISTS exercise_three;

CREATE TABLE exercise_three (
  id        integer NOT NULL auto_increment,
  name      varchar(50),
  gender    varchar(10),
  year_born integer(4),
  joined    integer(4),
  num_hires integer(15),
  PRIMARY KEY (id)
);

INSERT INTO exercise_three (name, gender, year_born, joined, num_hires)
VALUES ('Peter Jackson', 'male', '1961', '1997', '17000'),
       ('Jane Campion', 'female', '1954', '1980', '30000'),
       ('Roger Donaldson', 'male', '1945', '1980', '12000'),
       ('Temuera Morrison', 'male', '1960', '1995', '15500'),
       ('Russell Crowe', 'male', '1964', '1990', '10000'),
       ('Lucy Lawless', 'female', '1968', '1995', '5000'),
       ('Michael Hurst', 'male', '1957', '2000', '1500'),
       ('Andrew Niccol', 'male', '1964', '1997', '3500'),
       ('Kiri Te Kanawa', 'female', '1944', '1997', '500'),
       ('Lorde', 'female', '1996', '2010', '1000'),
       ('Scribe', 'male', '1979', '2000', '5000'),
       ('Kimbra', 'female', '1990', '2005', '7000'),
       ('Neil Finn', 'male', '1958', '1985', '6000'),
       ('Anika Moa', 'female', '1980', '2000', '700'),
       ('Bic Runga', 'female', '1976', '1995', '5000'),
       ('Ernest Rutherford', 'male', '1871', '1930', '4200');

