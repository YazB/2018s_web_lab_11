-- Answers to Exercise 8 here
DELETE FROM exercise_five WHERE fname='Beatrice';
ALTER TABLE exercise_five DROP COLUMN fname;
DROP TABLE exercise_five;

UPDATE exercise_six SET title='Dark Knight Rising' WHERE  title='Zoolander';
UPDATE exercise_six SET hire_cost=4 WHERE title='Raiders of the Lost Ark';

UPDATE exercise_seven SET article_id=2222 WHERE article_id=1111;